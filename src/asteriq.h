/* L'en-tête pour la bibliothèque « Asteriq »
 *
 * Les auteurs :
 *   Daniel McLaughlin  <ravenstar.8214@gmail.com>
 *
 * Copyright © 2018  Les Auteurs
 *
 * Sorti sous la license GNU GPL ; lisez le fichier « COPYING » pour
 * plus d'informations.
 */

#ifndef _ASTERIQ_H_
#define _ASTERIQ_H_

#include <inttypes.h>


// Définir le type des valeurs du retour
typedef uint32_t intret;

// Les valeurs du retour :
#define NORMALE \
        (intret)0b00000000000000000000000000000000
#define VALEUR_INAPPROPRIEE \
        (intret)0b00000000000000000000000000000001
#define VALEUR_INVALIDE \
        (intret)0b00000000000000000000000000000010
#define TABLEAU_NONINITIALISE \
        (intret)0b00000000000000000000000000000100
#define TABLEAU_INVALIDE \
        (intret)0b00000000000000000000000000001000
#define CALC_ITERATIVE \
        (intret)0b00000000000000000000000000010000
#define CALC_ERREUR \
        (intret)0b00000000000000000000000000100000

#define NONDEF07 \
        (intret)0b00000000000000000000000001000000
#define NONDEF08 \
        (intret)0b00000000000000000000000010000000
#define NONDEF09 \
        (intret)0b00000000000000000000000100000000
#define NONDEF10 \
        (intret)0b00000000000000000000001000000000
#define NONDEF11 \
        (intret)0b00000000000000000000010000000000
#define NONDEF12 \
        (intret)0b00000000000000000000100000000000
#define NONDEF13 \
        (intret)0b00000000000000000001000000000000
#define NONDEF14 \
        (intret)0b00000000000000000010000000000000
#define NONDEF15 \
        (intret)0b00000000000000000100000000000000
#define NONDEF16 \
        (intret)0b00000000000000001000000000000000
#define NONDEF17 \
        (intret)0b00000000000000010000000000000000
#define NONDEF18 \
        (intret)0b00000000000000100000000000000000
#define NONDEF19 \
        (intret)0b00000000000001000000000000000000
#define NONDEF20 \
        (intret)0b00000000000010000000000000000000
#define NONDEF21 \
        (intret)0b00000000000100000000000000000000
#define NONDEF22 \
        (intret)0b00000000001000000000000000000000
#define NONDEF23 \
        (intret)0b00000000010000000000000000000000
#define NONDEF24 \
        (intret)0b00000000100000000000000000000000
#define NONDEF25 \
        (intret)0b00000001000000000000000000000000
#define NONDEF26 \
        (intret)0b00000010000000000000000000000000
#define NONDEF27 \
        (intret)0b00000100000000000000000000000000
#define NONDEF28 \
        (intret)0b00001000000000000000000000000000
#define NONDEF29 \
        (intret)0b00010000000000000000000000000000
#define NONDEF30 \
        (intret)0b00100000000000000000000000000000
#define NONDEF31 \
        (intret)0b01000000000000000000000000000000
#define NONDEF32 \
        (intret)0b10000000000000000000000000000000


// Les fonction dans le module « interpolation.c »
intret interpl3(
  double dT1, // 1ère valeur
  double dT2, // 2ème valeur
  double dT3, // 3ème valeur
  double dFactor, // factor d'interpolation
  double* pdDiff1, // (retour) première différence [1ère]
  double* pdDiff2, // (retour) première différence [2ème]
  double* pdValeur // (retour) valeur interpolée de factor
);
intret extrmem3(
  double dT1, // 1ère valeur
  double dT2, // 2ème valeur
  double dT3, // 3ème valeur
  double* pdDiff1,    // (retour) première différence [1ère]
  double* pdDiff2,    // (retour) première différence [2ème]
  double* pdExtremum, // (retour) valeur d'extremum
  double* pdFactor    // (retour) factor d'interpolation d'extremum
);
intret absciss3(
  double dT1, // 1ère valeur
  double dT2, // 2ème valeur
  double dT3, // 3ème valeur
  double dValeur,  // valeur interpolée
  double* pdFactor // (retour) factor d'interpolation
);
intret interpl5(
  double dT1, // 1ère valeur
  double dT2, // 2ème valeur
  double dT3, // 3ème valeur
  double dT4, // 4ème valeur
  double dT5, // 5ème valeur
  double dFactor, // factor d'interpolation
  double* pdDiff1, // (retour) troisième différence [1ère]
  double* pdDiff2, // (retour) troisième différence [2ème]
  double* pdValeur // (retour) valeur interpolée de factor
);
intret extrmem5(
  double dT1, // 1ère valeur
  double dT2, // 2ème valeur
  double dT3, // 3ème valeur
  double dT4, // 4ème valeur
  double dT5, // 5ème valeur
  double* pdDiff1,    // (retour) troisième différence [1ère]
  double* pdDiff2,    // (retour) troisième différence [2ème]
  double* pdExtremum, // (retour) valeur d'extremum
  double* pdFactor    // (retour) factor d'interpolation d'extremum
);
intret absciss5(
  double dT1, // 1ère valeur
  double dT2, // 2ème valeur
  double dT3, // 3ème valeur
  double dT4, // 4ème valeur
  double dT5, // 5ème valeur
  double dValeur,  // valuer interpolée
  double* pdFactor // (retour) factor d'interpolation
);
intret interplgrng(
  double* pdaAbscisses, // tableau des absicisses
  double* pdaValeurs,   // tableau des valeurs
  int nNombreElements,  // nombre des elements dans les tableaux
  double dAbscisseDInterpolation, // absicisse d'interpolation
  double* pdValeur                // (retour) valeur interpolée
);

// Les fonctions dans le module « jourjulien.c »
intret dtval_grg(
  int nAnnee, // l'année
  int nMois,  // le mois
  int nJour   // le jour
);
intret dtval_jul(
  int nAnnee, // l'année
  int nMois,  // le mois
  int nJour   // le jour
);
intret grg_en_jj(
  int nAnnee,        // l'année
  int nMois,         // le mois
  int nJour,         // le jour
  double dFracDJour, // la fraction du jour
  double* pdJourJulien // (retour) le jour julien
);
intret jul_en_jj(
  int nAnnee,        // l'année
  int nMois,         // le mois
  int nJour,         // le jour
  double dFracDJour, // la fraction du jour
  double* pdJourJulien // (retour) le jour julien
);
intret jj_en_grg(
  double dJourJulien, // le jour julien
  int* pnAnnee,       // (retour) l'année
  int* pnMois,        // (retour) le mois
  int* pnJour,        // (retour) le jour
  double* pdFracDJour // (retour) la fraction du jour
);
intret jj_en_jul(
  double dJourJulien, // le jour julien
  int* pnAnnee,       // (retour) l'année
  int* pnMois,        // (retour) le mois
  int* pnJour,        // (retour) le jour
  double* pdFracDJour // (retour) la fraction du jour
);
intret jj_en_jjm(
  double dJourJulien,         // le jour julien
  double* pdJourJulienModifie // (retour) le jour julien modifié
);
intret jjm_en_jj(
  double dJourJulienModifie, // le jour julien modifié
  double* pdJourJulien       // (retour) le jour julien
);
intret jrdsemn(
  double dJourJulien,  // le jour julien
  int* pnJourDeSemaine // (retour) le jour de la semaine
);
intret jrdan(
  int nAnnee, // L'année
  int nMois,  // Le mois
  int nJour,  // Le jour
  int blGregorien, // Est-ce que le calendrier grégorien ?
                   // Si oui, 1 ; sinon, 0
  int* pnJourDAnnee // (retour) Le jour de l'année
);
intret jrdan_en_date(
  int nJourDAnnee,  // Le jour de l'année
  int blBissextile, // Est-ce que l'année bissextile ?
                    // Si oui, 1 ; sinon, 0
  int* pnMois, // (retour) Le mois
  int* pnJour  // (retour) Le jour
);
intret greg_en_juli(
  int nAnnee_Greg, // l'année en calendrier grégorien
  int nMois_Greg,  // le mois en calendrier grégorien
  int nJour_Greg,  // le jour en calendrier grégorien
  int* pnAnnee_Juli, // (retour) l'année en calendrier julien
  int* pnMois_Juli,  // (retour) le mois en calendrier julien
  int* pnJour_Juli   // (retour) le jour en calendrier julien
);
intret juli_en_greg(
  int nAnnee_Juli, // l'année en calendrier julien
  int nMois_Juli,  // le mois en calendrier julien
  int nJour_Juli,  // le jour en calendrier julien
  int* pnAnnee_Greg, // (retour) l'année en calendrier grégorien
  int* pnMois_Greg,  // (retour) le mois en calendrier grégorien
  int* pnJour_Greg   // (retour) le jour en calendrier grégorien
);













#endif // _ASTERIQ_H_

