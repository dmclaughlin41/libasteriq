/* Jour Julien
 *   Ce module implémente les fonctions de chapître 7 de le livre :
 *   Astronomical Algorithms
 *   par Jean Meeus
 *
 *
 * Les auteurs :
 *   Daniel McLaughlin  <ravenstar.8214@gmail.com>
 *
 * Copyright © 2018  Les Auteurs
 *
 * Sorti sous la license GNU GPL ; lisez le fichier « COPYING » pour
 * plus d'informations.
 */

// Inclure les en-têtes du système et l'en-tête de cette bibliothèque
#include <math.h>
#include "asteriq.h"


int bissextile_grg(int nAnnee) {
  int nRet;
  if (nAnnee % 400 == 0)
    nRet = 1;
  else if (nAnnee % 100 == 0)
    nRet = 0;
  else if (nAnnee % 4 == 0)
    nRet = 1;
  else
    nRet = 0;
  return nRet;
}



int bissextile_jul(int nAnnee) {
  return (nAnnee % 4 == 0) ? 1 : 0;
}



intret dtval_grg(int nAnnee, int nMois, int nJour) {
  intret nRet = NORMALE;

  // Est-ce que le jour moins de 1 ?
  if (nJour < 1)
    nRet |= VALEUR_INVALIDE;

  // Les moises de 31 jours
  if (nMois == 1 || nMois == 3 || nMois == 5 || nMois == 7 ||
                             nMois == 8 || nMois == 10 || nMois == 12) {
    if (nJour > 31)
      nRet |= VALEUR_INVALIDE;
  }

  // Les moises de 30 jours
  else if (nMois == 4 || nMois == 6 || nMois == 9 || nMois == 11) {
    if (nJour > 30)
      nRet |= VALEUR_INVALIDE;
  }

  // Le février
  else if (nMois == 2) {
    if (bissextile_grg(nAnnee)) {
      if (nJour > 29)
        nRet |= VALEUR_INVALIDE;
    } else {
      if (nJour > 28)
        nRet |= VALEUR_INVALIDE;
    }
  }

  // Le nombre du mois est invalid
  else {
    nRet |= VALEUR_INVALIDE;
  }

  // La date précéde-elle le début du calendrier grégorien ?
  //   (15 octobre 1582)
  if (nAnnee < 1582) {
    nRet |= VALEUR_INAPPROPRIEE;
  } else if (nAnnee == 1582) {
    if (nMois < 10) {
      nRet |= VALEUR_INAPPROPRIEE;
    } else if (nMois == 10) {
      if (nJour < 15)
        nRet |= VALEUR_INAPPROPRIEE;
    }
  }

  // Renvoyer la valeur du retour
  return nRet;
}



intret dtval_jul(int nAnnee, int nMois, int nJour) {
  intret nRet = NORMALE;

  // Est-ce que le jour moins de 1 ?
  if (nJour < 1)
    nRet |= VALEUR_INVALIDE;

  // Les moises de 31 jours
  if (nMois == 1 || nMois == 3 || nMois == 5 || nMois == 7 ||
                             nMois == 8 || nMois == 10 || nMois == 12) {
    if (nJour > 31)
      nRet |= VALEUR_INVALIDE;
  }

  // Les moises de 30 jours
  else if (nMois == 4 || nMois == 6 || nMois == 9 || nMois == 11) {
    if (nJour > 30)
      nRet |= VALEUR_INVALIDE;
  }

  // Le février
  else if (nMois == 2) {
    if (bissextile_jul(nAnnee)) {
      if (nJour > 29)
        nRet |= VALEUR_INVALIDE;
    } else {
      if (nJour > 28)
        nRet |= VALEUR_INVALIDE;
    }
  }

  // Le nombre du mois est invalid
  else {
    nRet |= VALEUR_INVALIDE;
  }

  // Renvoyer la valeur du retour
  return nRet;
}



intret grg_en_jj(int nAnnee, int nMois, int nJour, double dFracDJour,
                                                 double* pdJourJulien) {
  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la date valide ?
  nRet |= dtval_grg(nAnnee, nMois, nJour);
  if (dFracDJour < 0 || dFracDJour >= 1)
    nRet |= VALEUR_INVALIDE;
  if ((nRet & VALEUR_INVALIDE) == VALEUR_INVALIDE)
    return nRet; // si invalide, renvoyer ici

  // Prendre les valeurs de la date pour traiter
  int nAn, nMs;
  nAn = nAnnee;
  nMs = nMois;
  double dJr;
  dJr = (double)nJour + dFracDJour;

  // Calculer le jour julien
  if (nMs < 3) {
    nAn -= 1;
    nMs += 12;
  }
  int nA, nB;
  nA = (int)floor(nAn / 100);
  nB = 2 - nA + (int)floor(nA / 4);
  *pdJourJulien = floor(365.25 * (nAn + 4716)) +
                  floor(30.6001 * (nMs + 1)) +
                  (double)nB + dJr - 1524.5;

  // Renvoyer la valeur du retour
  return nRet;
}



intret jul_en_jj(int nAnnee, int nMois, int nJour, double dFracDJour,
                                                 double* pdJourJulien) {
  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la date valide ?
  nRet |= dtval_jul(nAnnee, nMois, nJour);
  if (dFracDJour < 0 || dFracDJour >= 1)
    nRet |= VALEUR_INVALIDE;
  if ((nRet & VALEUR_INVALIDE) == VALEUR_INVALIDE)
    return nRet; // si invalide, renvoyer ici

  // Prendre les valeurs de la date pour traiter
  int nAn, nMs;
  nAn = nAnnee;
  nMs = nMois;
  double dJr;
  dJr = (double)nJour + dFracDJour;

  // Calculer le jour julien
  if (nMs < 3) {
    nAn -= 1;
    nMs += 12;
  }
  *pdJourJulien = floor(365.25 * (nAn + 4716)) +
                  floor(30.6001 * (nMs + 1)) +
                  dJr - 1524.5;

  // Renvoyer la valeur du retour
  return nRet;
}



intret jj_en_grg(double dJourJulien, int* pnAnnee, int* pnMois,
                                     int* pnJour, double* pdFracDJour) {
  intret nRet = NORMALE;

  // Est-ce que le jour julien moins de 0 ?
  if (dJourJulien < 0)
    nRet |= VALEUR_INVALIDE;

  // Le jour julien précéde-il le début du calendrier grégorien ?
  if (dJourJulien < 2299161)
    nRet |= VALEUR_INAPPROPRIEE;

  // Prendre le jour julien et la fraction du jour pour traiter
  double dJrDJour = dJourJulien + 0.5;
  int nJrDJour = (int)floor(dJrDJour);
  double dFracDJour = dJrDJour - (double)nJrDJour;

  // Calculer les valeurs pour le calcul de la date
  int nA, nB, nC, nD, nE;
  int nAlpha = (int)floor((double)(nJrDJour - 1867216.25) / 36524.25);
  nA = nJrDJour + 1 + nAlpha - (int)floor((double)nAlpha / 4);
  nB = nA + 1524;
  nC = (int)floor(((double)nB - 122.1) / 365.25);
  nD = (int)floor(365.25 * nC);
  nE = (int)floor((double)(nB - nD) / 30.6001);

  // Calculer la date
  int nAn, nMs, nJr;
  nJr = nB - nD - (int)floor(30.6001 * nE);
  if (nE < 14)
    nMs = nE - 1;
  else
    nMs = nE - 13;
  if (nMs > 2)
    nAn = nC - 4716;
  else
    nAn = nC - 4715;

  // Est-ce que la date valide ?
  if ((dtval_grg(nAn, nMs, nJr) & VALEUR_INVALIDE) == VALEUR_INVALIDE)
    nRet |= CALC_ERREUR;

  // Mettre la date à leurs pointeurs
  *pnAnnee = nAn;
  *pnMois = nMs;
  *pnJour = nJr;
  *pdFracDJour = dFracDJour;

  // Renvoyer la valeur du retour
  return nRet;
}



intret jj_en_jul(double dJourJulien, int* pnAnnee, int* pnMois,
                                     int* pnJour, double* pdFracDJour) {
  intret nRet = NORMALE;

  // Est-ce que le jour julien moins de 0 ?
  if (dJourJulien < 0)
    nRet |= VALEUR_INVALIDE;

  // Prendre le jour julien et la fraction du jour pour traiter
  double dJrDJour = dJourJulien + 0.5;
  int nJrDJour = (int)floor(dJrDJour);
  double dFracDJour = dJrDJour - (double)nJrDJour;

  // Calculer les valeurs pour le calcul de la date
  int nA, nB, nC, nD, nE;
  nA = nJrDJour;
  nB = nA + 1524;
  nC = (int)floor(((double)nB - 122.1) / 365.25);
  nD = (int)floor(365.25 * nC);
  nE = (int)floor((double)(nB - nD) / 30.6001);

  // Calculer la date
  int nAn, nMs, nJr;
  nJr = nB - nD - (int)floor(30.6001 * nE);
  if (nE < 14)
    nMs = nE - 1;
  else
    nMs = nE - 13;
  if (nMs > 2)
    nAn = nC - 4716;
  else
    nAn = nC - 4715;

  // Est-ce que la date valide ?
  if ((dtval_jul(nAn, nMs, nJr) & VALEUR_INVALIDE) == VALEUR_INVALIDE)
    nRet |= CALC_ERREUR;

  // Mettre la date à leurs pointeurs
  *pnAnnee = nAn;
  *pnMois = nMs;
  *pnJour = nJr;
  *pdFracDJour = dFracDJour;

  // Renvoyer la valeur du retour
  return nRet;
}



intret jj_en_jjm(double dJourJulien, double* pdJourJulienModifie) {
  *pdJourJulienModifie = dJourJulien - 2400000.5;
  return NORMALE;
}



intret jjm_en_jj(double dJourJulienModifie, double* pdJourJulien) {
  *pdJourJulien = dJourJulienModifie + 2400000.5;
  return NORMALE;
}



intret jrdsemn(double dJourJulien, int* pnJourDeSemaine) {

  // Prendre le jour julien à 0h
  double dJr = floor(dJourJulien - 0.5) + 0.5;

  // Ajouter 1,5
  int nJr = (int)(dJr + 1.5);

  // Calculer le modulo en base de 7
  nJr %= 7;

  // Si le résultat est moins de zéro, rajouter 7
  if (nJr < 0)
    nJr += 7;

  // Mettre le jour de la semaine à leur pointeur et renvoyer la valeur
  // du retour
  *pnJourDeSemaine = nJr;
  return NORMALE;
}



intret jrdan(int nAnnee, int nMois, int nJour, int blGregorien,
                                                    int* pnJourDAnnee) {
  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la date valide ?
  if (blGregorien == 0)
    nRet |= dtval_jul(nAnnee, nMois, nJour);
  else
    nRet |= dtval_grg(nAnnee, nMois, nJour);

  // Est-ce que l'année bissextile ?
  int nK;
  if (blGregorien == 0)
    nK = (bissextile_jul(nAnnee)) ? 1 : 2;
  else
    nK = (bissextile_grg(nAnnee)) ? 1 : 2;

  // Calculer le jour de l'année
  int nJrDAn = (int)floor((double)(275 * nMois) / 9) -
               nK * (int)floor((double)(nMois + 9) / 12) + nJour - 30;

  // Est-ce que le jour de l'année un nombre valid ?
  if (nJrDAn < 1)
    nRet |= CALC_ERREUR;
  if (nK == 1) { // année bissextile
    if (nJrDAn > 366)
      nRet |= CALC_ERREUR;
  } else { // année ordinaire
    if (nJrDAn > 365)
      nRet |= CALC_ERREUR;
  }

  // Mettre le jour de l'année à leur pointeur
  *pnJourDAnnee = nJrDAn;

  // Renvoyer la valeur du retour
  return nRet;
}



intret jrdan_en_date(int nJourDAnnee, int blBissextile, int* pnMois,
                                                          int* pnJour) {
  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que le nombre du jour de l'année valid ?
  if (nJourDAnnee < 1)
    nRet |= VALEUR_INVALIDE;
  if (blBissextile == 0) {
    if (nJourDAnnee > 365)
      nRet |= VALEUR_INVALIDE;
  } else {
    if (nJourDAnnee > 366)
      nRet |= VALEUR_INVALIDE;
  }

  // Est-ce que l'année bissextile ?
  int nK = (blBissextile == 0) ? 2 : 1;

  // Calculer le mois
  int nMs;
  if (nJourDAnnee < 32) {
    nMs = 1;
  } else {
    nMs = (int)floor(((double)(9 * (nK + nJourDAnnee)) / 275) + 0.98);
  }

  // Calculer le jour
  int nJr = nJourDAnnee - (int)floor((double)(275 * nMs) / 9) +
            nK * (int)floor((double)(nMs + 9) / 12) + 30;

  // Est-ce que la date valide ?
  intret nValide;
  if (blBissextile == 0) {
    nValide = dtval_jul(2001, nMs, nJr);
  } else {
    nValide = dtval_jul(2000, nMs, nJr);
  }
  // Cette fonction ne prend pas la valeur de l'année mais les fonctions
  // de validation de date réquise elle. L'année « 2000 » est bissextile
  // et l'année « 2001 » n'est pas bissextile. Ces années sont utilisées
  // seulement pour la validation comme bissextile ou ordinaire.

  // Renvoyer la valeur du retour
  return nRet;
}



intret greg_en_juli(int nAnnee_Greg, int nMois_Greg, int nJour_Greg,
                int* pnAnnee_Juli, int* pnMois_Juli, int* pnJour_Juli) {

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la date grégorienne valide ?
  nRet |= dtval_grg(nAnnee_Greg, nMois_Greg, nJour_Greg);
  if ((nRet & VALEUR_INVALIDE) == VALEUR_INVALIDE)
    return nRet;

  // Convertir la date grégorienne en jours juliens
  double dJrJul, dFracDJour;
  dFracDJour = (double)0.00;
  nRet |= grg_en_jj(nAnnee_Greg, nMois_Greg, nJour_Greg, dFracDJour,
                                                               &dJrJul);
  // Et convertir le jour julien en date julienne
  nRet |= jj_en_jul(dJrJul, pnAnnee_Juli, pnMois_Juli, pnJour_Juli,
                                                           &dFracDJour);
  // Renvoyer la valeur du retour
  return nRet;
}



intret juli_en_greg(int nAnnee_Juli, int nMois_Juli, int nJour_Juli,
                int* pnAnnee_Greg, int* pnMois_Greg, int* pnJour_Greg) {

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la date julienne valide ?
  nRet |= dtval_jul(nAnnee_Juli, nMois_Juli, nJour_Juli);
  if ((nRet & VALEUR_INVALIDE) == VALEUR_INVALIDE)
    return nRet;

  // Convertir la date julienne en jours juliens
  double dJrJul, dFracDJour;
  dFracDJour = (double)0.00;
  nRet |= jul_en_jj(nAnnee_Juli, nMois_Juli, nJour_Juli, dFracDJour,
                                                               &dJrJul);
  // Et convertir le jour julien en date grégorienne
  nRet |= jj_en_grg(dJrJul, pnAnnee_Greg, pnMois_Greg, pnJour_Greg,
                                                           &dFracDJour);
  // Renvoyer la valeur du retour
  return nRet;
}

