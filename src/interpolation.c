/* Interpolation
 *   Ce module implémente les fonctions de chapître 3 de le livre :
 *   Astronomical Algorithms
 *   par Jean Meeus
 *
 *
 * Les auteurs :
 *   Daniel McLaughlin  <ravenstar.8214@gmail.com>
 *
 * Copyright © 2018  Les Auteurs
 *
 * Sorti sous la license GNU GPL ; lisez le fichier « COPYING » pour
 * plus d'informations.
 */

// Inclure les en-têtes du système et l'en-tête de cette bibliothèque
#include "asteriq.h"


void intrdiff3(double d1, double d2, double d3, double* pdA,
                                             double* pdB, double* pdC) {
  // Calculer les premières différences
  *pdA = d2 - d1;
  *pdB = d3 - d2;

  // Calculer la deuxième différence
  *pdC = *pdB - *pdA;
}



intret interpl3(double dT1, double dT2, double dT3, double dFactor,
                   double* pdDiff1, double* pdDiff2, double* pdValeur) {

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la valeur de factor invalide ou inapproprie ?
  if (dFactor < -1 || dFactor > 1)
    nRet |= VALEUR_INVALIDE;
  else if (dFactor < -0.5 || dFactor > 0.5)
    nRet |= VALEUR_INAPPROPRIEE;

  // Calculer les différances
  double dA, dB, dC;
  intrdiff3(dT1, dT2, dT3, &dA, &dB, &dC);

  // Mettre les différances dans leurs pointeurs
  *pdDiff1 = dA;
  *pdDiff2 = dB;

  // Interpoler la valeur du résultat
  //   L'équation : y = y2 + (n/2)*(a + b + nc)
  double dRes = dA + dB + (dC * dFactor);
  dRes *= dFactor / (double)2;
  dRes += dT2;

  // Mettre le résultat d'interpolation dans leur pointeur
  *pdValeur = dRes;

  // Renvoyer la valeur du retour
  return nRet;
}



intret extrmem3(double dT1, double dT2, double dT3, double* pdDiff1,
                double* pdDiff2, double* pdExtremum, double* pdFactor) {

  // Est-ce que la valeur dT2 plus grande ou plus petite de dT1 et dT2 ?
  if (dT2 < dT1 && dT2 > dT3)
    return VALEUR_INVALIDE;
  if (dT2 > dT1 && dT2 < dT3)
    return VALEUR_INVALIDE;

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Calculer les différances
  double dA, dB, dC;
  intrdiff3(dT1, dT2, dT3, &dA, &dB, &dC);

  // Mettre les différances primaires dans leurs pointeurs
  *pdDiff1 = dA;
  *pdDiff2 = dB;

  // Calculer l'extremum et mettre le résultat dans leur pointeur
  //   L'équation : ym = y2 - [(a+b)^2 / 8c]
  double dRes = dA + dB;
  dRes *= dRes;
  dRes /= (double)8 * dC;
  dRes = dT2 - dRes;
  *pdExtremum = dRes;

  // Calculer la valeur de factor de l'extremum et mettre le résultat
  // dans leur pointeur
  //   L'équation : nm = -[(a + b) / 2c]
  dRes = dA + dB;
  dRes /= (double)2 * dC;
  *pdFactor = -dRes;

  // Est-ce que la factor inappropriée ou invalide ?
  if (dRes < 0) dRes = -dRes;
  if (dRes > 1) nRet |= VALEUR_INVALIDE;
  else if (dRes > 0.5) nRet |= VALEUR_INAPPROPRIEE;

  // Renvoyer la valeur du retour
  return nRet;
}



intret absciss3(double dT1, double dT2, double dT3, double dValeur,
                                                     double* pdFactor) {
  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la valeur pour trouver en dehors de la plage des valeurs
  // données ?
  int blDansPlage = 0;
  if (dValeur <= dT1 && dValeur >= dT2) blDansPlage = 1;
  if (dValeur >= dT1 && dValeur <= dT2) blDansPlage = 1;
  if (dValeur <= dT2 && dValeur >= dT3) blDansPlage = 1;
  if (dValeur >= dT2 && dValeur <= dT3) blDansPlage = 1;
  if (blDansPlage == 0)
    nRet |= VALEUR_INAPPROPRIEE;

  // Calculer les différances
  double dA, dB, dC;
  intrdiff3(dT1, dT2, dT3, &dA, &dB, &dC);

  // Calculer l'abscisse de la valeur
  //   L'équation originale : n_1 = -2y_2 / ( a + b + cn_0 )
  //   L'équation modifiée : n_1 = ( 2k - 2y_2 ) / ( a + b + cn_0 )
  //     ces équations sont itératives
  //
  // La nouvelle équation calcule l'abscisse pour la valeur ; l'ancienne
  // équation calcule l'abscisse pour zéro
  double dFact0, dFact1;
  double dDiff0, dDiff1;
  int blFini = 0;
  dFact0 = (double)0;
  dFact1 = ((2 * dValeur) - (2 * dT2)) / (dA + dB + (dC * dFact0));
  dDiff0 = dFact1 - dFact0;
  if (dDiff0 < 0) dDiff0 = -dDiff0;
  dFact0 = dFact1;
  do {
    dFact1 = ((2 * dValeur) - (2 * dT2)) / (dA + dB + (dC * dFact0));
    dDiff1 = dFact1 - dFact0;
    if (dDiff1 < 0) dDiff1 = -dDiff1;
    if (dDiff1 > dDiff0) { // si l'itération diverge ...
      blFini = 1; // ... annuler l'operation itérative
      nRet |= CALC_ITERATIVE; // ... signaler l'erreur
      return nRet; // ... et s'arrêter la fonction ici
    }
    dFact0 = dFact1;
    if (dDiff1 == 0) blFini = 1;
  } while (blFini == 0);

  // Mettre le résultat d'itération à leur pointeur
  *pdFactor = dFact1;

  // Est-ce que la factor inappropriée ou invalide ?
  if (dFact1 < 0) dFact1 = -dFact1;
  if (dFact1 > 1) nRet |= VALEUR_INVALIDE;
  else if (dFact1 > 0.5) nRet |= VALEUR_INAPPROPRIEE;

  // Renvoyer la valeur du retour
  return nRet;
}



void intrdiff5(double d1, double d2, double d3, double d4, double d5,
            double* pdA, double* pdB, double* pdC, double* pdD,
            double* pdE, double* pdF, double* pdG, double* pdH,
                                             double* pdJ, double* pdK) {
  // Calculer les premières différences
  *pdA = d2 - d1;
  *pdB = d3 - d2;
  *pdC = d4 - d3;
  *pdD = d5 - d4;

  // Calculer les deuxièmes différences
  *pdE = *pdB - *pdA;
  *pdF = *pdC - *pdB;
  *pdG = *pdD - *pdC;

  // Calculer les troisièmes différences
  *pdH = *pdF - *pdE;
  *pdJ = *pdG - *pdF;

  // Calculer la quatrième différence
  *pdK = *pdJ - *pdH;
}



intret interpl5(double dT1, double dT2, double dT3, double dT4,
            double dT5, double dFactor, double* pdDiff1,
                                    double* pdDiff2, double* pdValeur) {

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la valeur de factor invalide ou inapproprie ?
  if (dFactor < -1 || dFactor > 1)
    nRet |= VALEUR_INVALIDE;
  else if (dFactor < -0.5 || dFactor > 0.5)
    nRet |= VALEUR_INAPPROPRIEE;

  // Calculer les différances
  double dA, dB, dC, dD, dE, dF, dG, dH, dJ, dK;
  intrdiff5(dT1, dT2, dT3, dT4, dT5, &dA, &dB, &dC, &dD, &dE, &dF, &dG,
                                                         &dH, &dJ, &dK);

  // Mettre les différances dans leurs pointeurs
  *pdDiff1 = dH;
  *pdDiff2 = dJ;

  // Interpoler la valeur du résultat
  //   L'équasion :
  //   y = y3 + (n/2)(B+C) + (n²/2)F + [n(n²-1)/12](H+J) +
  //                                                      [n²(n²-1)/24]K
  double dTrm2 = dFactor * (dB + dC) / (double)2;
  double dTrm3 = dFactor * dFactor * dF / (double)2;
  double dTrm4 = dFactor * (dFactor * dFactor - (double)1) *
                                                 (dH + dJ) / (double)12;
  double dTrm5 = dFactor * dFactor * (dFactor * dFactor - (double)1) *
                                                        dK / (double)24;
  double dRes = dT3 + dTrm2 + dTrm3 + dTrm4 + dTrm5;

  // Mettre le résultat d'interpolation dans leur pointeur
  *pdValeur = dRes;

  // Renvoyer la valeur du retour
  return nRet;
}



intret extrmem5(double dT1, double dT2, double dT3, double dT4,
            double dT5, double* pdDiff1, double* pdDiff2,
                                 double* pdExtremum, double* pdFactor) {

  // Est-ce que la valeur de dT3 plus grande ou plus petite de la
  // valeurs dT1 et dT2 et dT4 et dT5 ?
  int blValid = 0;
  if (dT3 >= dT2 && dT2 >= dT1 && dT3 >= dT4 && dT4 >= dT5) blValid = 1;
  if (dT3 <= dT2 && dT2 <= dT1 && dT3 <= dT4 && dT4 <= dT5) blValid = 1;
  if (blValid == 0)
    return VALEUR_INVALIDE;

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Calculer les différences
  double dA, dB, dC, dD, dE, dF, dG, dH, dJ, dK;
  intrdiff5(dT1, dT2, dT3, dT4, dT5, &dA, &dB, &dC, &dD, &dE, &dF, &dG,
                                                         &dH, &dJ, &dK);
  // Mettre les différences dans leurs pointeurs
  *pdDiff1 = dH;
  *pdDiff2 = dJ;

  // Calculer la factor d'interpolation
  //   L'équation de la factor :
  //   nm = (6B + 6C - H - J + 3nm²[H+J] + 2nm³K) / (K - 12F)
  //   (itérative)
  double dFact0, dFact1;
  double dDiff0, dDiff1;
  double dTrm1, dTrm2, dTrm3, dTrm4;
  int blFini = 0;
  dFact0 = (double)0;
  dTrm1 = (double)6 * dB;
  dTrm2 = (double)6 * dC;
  dTrm3 = (double)3 * dFact0 * dFact0 * (dH + dJ);
  dTrm4 = (double)2 * dFact0 * dFact0 * dFact0 * dK;
  dFact1 = (dTrm1 + dTrm2 - dH - dJ + dTrm3 + dTrm4) /
                                               (dK - ((double)12 * dF));
  dDiff0 = dFact1 - dFact0;
  if (dDiff0 < 0) dDiff0 = -dDiff0;
  dFact0 = dFact1;
  do {
    dTrm3 = (double)3 * dFact0 * dFact0 * (dH + dJ);
    dTrm4 = (double)2 * dFact0 * dFact0 * dFact0 * dK;
    dFact1 = (dTrm1 + dTrm2 - dH - dJ + dTrm3 + dTrm4) /
                                               (dK - ((double)12 * dF));
    dDiff1 = dFact1 - dFact0;
    if (dDiff1 < 0) dDiff1 = -dDiff1;
    if (dDiff1 > dDiff0) { // si l'itération diverge ...
      blFini = 1; // ... annuler l'opération iterative
      nRet |= CALC_ITERATIVE; // ... signaler l'erreur
      return nRet; // ... et s'arrêter la fonction ici
    }
    dFact0 = dFact1;
    if (dDiff1 == 0) blFini = 1;
  } while (blFini == 0);

  // Utiliser l'itération pour le calcul de la valeur d'extremum
  nRet |= interpl5(dT1, dT2, dT3, dT4, dT5, dFact0, &dDiff0, &dDiff1,
                                                            pdExtremum);
  // Mettre la factor d'interpolation à leur pointeur
  *pdFactor = dFact0;
  // la valeur déjà mis à leur pointeur par la fonction au-dessus
  // [interpl5]

  // Est-ce que la factor inappropriée ou invalide ?
  if (dFact0 < 0) dFact0 = -dFact0;
  if (dFact0 > 1) nRet |= VALEUR_INVALIDE;
  else if (dFact0 > 0.5) nRet |= VALEUR_INAPPROPRIEE;

  // Renvoyer la valeur du retour
  return nRet;
}



intret absciss5(double dT1, double dT2, double dT3, double dT4,
                         double dT5, double dValeur, double* pdFactor) {

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que la valeur pour trouver en dehors de la plage des valeurs
  // données ?
  int blDansPlage = 0;
  if (dValeur <= dT1 && dValeur >= dT2) blDansPlage = 1;
  if (dValeur >= dT1 && dValeur <= dT2) blDansPlage = 1;
  if (dValeur <= dT2 && dValeur >= dT3) blDansPlage = 1;
  if (dValeur >= dT2 && dValeur <= dT3) blDansPlage = 1;
  if (dValeur <= dT3 && dValeur >= dT4) blDansPlage = 1;
  if (dValeur >= dT3 && dValeur <= dT4) blDansPlage = 1;
  if (dValeur <= dT4 && dValeur >= dT5) blDansPlage = 1;
  if (dValeur >= dT4 && dValeur <= dT5) blDansPlage = 1;
  if (blDansPlage == 0)
    nRet |= VALEUR_INAPPROPRIEE;

  // Calculer les différences
  double dA, dB, dC, dD, dE, dF, dG, dH, dJ, dK;
  intrdiff5(dT1, dT2, dT3, dT4, dT5, &dA, &dB, &dC, &dD, &dE, &dF, &dG,
                                                         &dH, &dJ, &dK);
  // Calculer l'abscesse de la valeur
  //   L'équation :
  //     n_(n+1) = [-24y3+n²(K-12F)-2n³(H+J)-n⁴K] / [2(6B+6C-H-J)]
  //     (itérative)
  double dFact0, dFact1;
  double dDiff0, dDiff1;
  int blFini = 0;
  dFact0 = (double)0;
  dFact1 = (
      24 * dValeur - 24 * dT3 +
      dFact0 * dFact0 * (dK - 12 * dF) -
      2 * dFact0 * dFact0 * dFact0 * (dH + dJ) -
      dFact0 * dFact0 * dFact0 * dFact0 * dK
    ) / (12 * dB + 12 * dC - 2 * dH - 2 * dJ);
  dDiff0 = dFact1 - dFact0;
  if (dDiff0 < 0) dDiff0 = -dDiff0;
  dFact0 = dFact1;
  do {
    dFact1 = (
        24 * dValeur -  24 * dT3 +
        dFact0 * dFact0 * (dK - 12 * dF) -
        2 * dFact0 * dFact0 * dFact0 * (dH + dJ) -
        dFact0 * dFact0 * dFact0 * dFact0 * dK
      ) / (12 * dB + 12 * dC - 2 * dH - 2 * dJ);
    dDiff1 = dFact1 - dFact0;
    if (dDiff1 < 0) dDiff1 = -dDiff1;
    if (dDiff1 > dDiff0) { // si l'itération diverge ...
      blFini = 1; // ... annuler l'operation itérative
      nRet |= CALC_ITERATIVE; // ... signaler l'erreur
      return nRet; // ... et s'arrêter la fonction ici
    }
    dFact0 = dFact1;
    if (dDiff1 == 0) blFini = 1;
  } while (blFini == 0);

  // Mettre le résultat d'itération à leur pointeur
  *pdFactor = dFact1;

  // Est-ce que la factor inappropriée ou invalide ?
  if (dFact1 < 0) dFact1 = -dFact1;
  if (dFact1 > 1) nRet |= VALEUR_INVALIDE;
  else if (dFact1 > 0.5) nRet |= VALEUR_INAPPROPRIEE;

  // Renvoyer la valeur du retour
  return nRet;
}



intret interplgrng(double* pdaAbscisses, double* pdaValeurs,
            int nNombreElements, double dAbscisseDInterpolation,
                                                     double* pdValeur) {

  // Sont les tableaux des absicisses et valeurs initialises ?
  if (pdaAbscisses == (double*)0) return TABLEAU_NONINITIALISE;
  if (pdaValeurs == (double*)0) return TABLEAU_NONINITIALISE;

  // Signale le nombre des elements qu'il y a au moins deux absicisses ?
  if (nNombreElements < 2)
    return VALEUR_INVALIDE;

  // Tous les absicisses, sont-ils uniques ?
  int blUniques = 1;
  int i = 0, j = 0;
  do {
    j = i + 1;
    do {
      if (*(pdaAbscisses + i) == *(pdaAbscisses + j))
        blUniques = 0;
      j++;
    } while (j < nNombreElements);
    i++;
  } while (i < nNombreElements);
  if (blUniques == 0)
    return VALEUR_INVALIDE;

  // Quelle est la plage des absicisses ?
  double dValMin, dValMax;
  dValMin = dValMax = *(pdaAbscisses + 0);
  i = 1;
  do {
    if (*(pdaAbscisses + i) > dValMax) dValMax = *(pdaAbscisses + i);
    if (*(pdaAbscisses + i) < dValMin) dValMin = *(pdaAbscisses + i);
    i++;
  } while (i < nNombreElements);

  // Initialiser la valeur du retour
  intret nRet = NORMALE;

  // Est-ce que l'abscisse entre la minimum et la maximum ?
  if (dAbscisseDInterpolation < dValMin) nRet |= VALEUR_INAPPROPRIEE;
  if (dAbscisseDInterpolation > dValMax) nRet |= VALEUR_INAPPROPRIEE;

  // Calculer la valeur d'interpolation
  //   L'équation :
  //     y = y_1 L_1 + y_2 L_2 + y_3 L+3 + ... + y_n L_n
  //
  //       où
  //            n     x - x_j
  //     L_i =  ∏    ---------
  //           j=1   x_i - x_j
  //           j≠i
  double dL;
  *pdValeur = (double)0; // initialiser le pointeur de résultat à zero
  i = 1;
  do {
    dL = (double)1;
    j = 1;
    do {
      if (j != i)
        dL *= (dAbscisseDInterpolation - *(pdaAbscisses + j)) /
              (*(pdaAbscisses + i) - *(pdaAbscisses + j));
      j++;
    } while (j < nNombreElements);
    *pdValeur += dL * *(pdaValeurs + i);
    i++;
  } while (i < nNombreElements);

  // Renvoyer la valeur du retour
  return nRet;
}

